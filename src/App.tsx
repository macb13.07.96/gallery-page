import React from 'react';
import {GalleryList} from "./components/GalleryList";

function App() {
  return (
    <>
      <GalleryList/>
    </>
  );
}

export default App;
