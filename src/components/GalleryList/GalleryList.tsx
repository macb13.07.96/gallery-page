import React, {useEffect, useState} from 'react';
import axios from 'axios';
import './GalleryList.css'
import {FullscreenImage} from "../FullscreenImage";

const apiKey = '43854550-099158840c188abb07f9f8265';
const perPage = 20;
const baseURL = 'https://pixabay.com/api/';

export interface Image {
    previewURL: string;
    tags: string;
    likes: number;
    views: number;
    largeImageURL: string;
}

export const GalleryList: React.FC = () => {
    const [images, setImages] = useState<Image[]>([]);
    const [selectedImage, setSelectedImage] = useState<Image | null>(null);

    useEffect(() => {
        axios.get(`${baseURL}?key=${apiKey}&per_page=${perPage}`)
            .then(response => {
                setImages(response.data.hits);
            })
            .catch(error => {
                console.error('Ошибка загрузки изображений:', error);
            });
    }, []);

    const openImageFullscreen = (image: Image) => {
        setSelectedImage(image);
    };

    const closeImageFullscreen = () => {
        setSelectedImage(null);
    };


    return (
        <div className='gallery'>
            {images.map((image, index) => (
                <div key={index} className='image-container'>
                    <img
                        src={image.previewURL}
                        alt={image.tags}
                        onClick={() => openImageFullscreen(image)}
                        className='image'
                    />
                    <div className='image-info'>
                        <p>Likes: {image.likes}</p>
                        <p>Views: {image.views}</p>
                    </div>
                </div>

            ))}

            <FullscreenImage image={selectedImage} onClose={closeImageFullscreen} />
        </div>
    );
};

