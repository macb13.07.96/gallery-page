import React from "react";
import {Image} from "../GalleryList/GalleryList";
import './FullscreenImage.css'

interface FullscreenImageProps {
    image: Image | null;
    onClose: () => void;
}
export const FullscreenImage: React.FC<FullscreenImageProps> = ({ image, onClose }) => {
    if (!image) {
        return null;
    }

    return (
        <div className='fullscreen'>
            <span className="close-button" onClick={onClose}>✕</span>
            <img
                src={image.largeImageURL}
                alt={image.tags}
                className='fullscreen-image'
                onClick={onClose}
            />
        </div>
    );
}



